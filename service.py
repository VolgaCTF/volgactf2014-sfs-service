#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import struct
import json
from datetime import datetime
import socket
import os
import SocketServer
from modules.SFS import SFS, SfsException, generate_SFS_password
from modules.CryptoUtils import CryptoUtils, CryptoException, generate_RSA_keys



sfs = None
CU = None
logger = None


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	pass
class ServiceServerHandler(SocketServer.BaseRequestHandler):
	def __init__(self, request, client_address, server):
		SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)


	def handle(self):
		logger.info('Accepted  connection from %s' % self.client_address[0])
		
		try:
			# authorise checker
			connector = Connector(self.request)
			(session_key, client_name) = CU.authorise(connector)
			
			# retrieve and execute command
			message = CU.decrypt(connector.read_message(), session_key)
			(cmd, args) = parse_message(message)
			logger.info('Client ''%s'' is to execute command %s' % (client_name, cmd))
			
			if cmd == 'PUT':
				if len(args) != 2:
					raise Exception('ERROR: put requires two arguments')
				j = int(args[0])
				sfs.embed_file(j, args[1])
				connector.send_message(CU.encrypt('DONE', session_key))

			elif cmd == 'GET':
				if len(args) != 1:
					raise Exception('ERROR: get requires one argument')
				j = int(args[0])
				F = sfs.extract_file(j)
				connector.send_message(CU.encrypt(F, session_key))
				
			else:
				raise Exception('ERROR: unknown command')
			logger.info('Client \'%s\' has successfully executed command %s' % (client_name, cmd))

		except Exception as ex:
			logger.error(str(ex), exc_info=True)
		finally:
			logger.info('Processed connection from %s' % self.client_address[0])
		return


#
# helper functions
#
def parse_message(message):
	args = message.split(' ')
	cmd = args[0].strip()
	del args[0]
	return (cmd, args)

class Connector:
	def __init__(self, s):
		self.s = s
	def read_message(self):
		received_buffer = self.s.recv(4)
		if len(received_buffer) < 4:
			raise Exception('Error while receiving data')
		to_receive = struct.unpack('>I', received_buffer[0:4])[0]
		received_buffer = ''
		while (len(received_buffer) < to_receive):
			received_buffer += self.s.recv(to_receive - len(received_buffer))
		return received_buffer
		
	def send_message(self, message):
		send_buffer = struct.pack('>I', len(message)) + message
		self.s.sendall(send_buffer)


def read_settings(settings_file):
	with open(settings_file, 'r') as f:
		settings = json.loads(f.read())
	if not 'servicePrivateFileName' in settings:
		settings['servicePrivateFileName'] = 'service.private'
	if not 'pathToSFS' in settings:
		settings['pathToSFS'] = '/StegFS/files'
	
	
	if not 'keyGenerationPassword' in settings  or  not os.path.isfile(settings['servicePrivateFileName']) or not 'serviceName' in settings:
		#ask checker for the missing information
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect((settings['checkerIP'], settings['checkerPort']))
			
			# generate SFS password and RSA keys
			(public_key, private_key) = generate_RSA_keys()
			with open(settings['servicePrivateFileName'], 'wb+') as f:
				f.write(private_key)
			sfsPass = generate_SFS_password()
			settings['keyGenerationPassword'] = sfsPass
			additional_settings = {
				'publicKey': public_key,
				'keyGenerationPassword': sfsPass
			}
			connector = Connector(s)
			connector.send_message(json.dumps(additional_settings))
			settings['serviceName'] = connector.read_message()
			with open(settings_file, 'w') as f:
				f.write(json.dumps(settings))
		finally:
			s.close()
	return settings







if __name__ == '__main__':
	try:
		settings = read_settings('settings.json')
		logger = logging.getLogger(__name__)
		logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)#filename=settings['logFileName'])
		sfs = SFS(settings['numOfCVs'], settings['maxFiles'], settings['fileLength'], settings['pathToSFS'], settings['keyGenerationPassword'])
		CU = CryptoUtils(settings['serviceName'], settings['pathToPublicKeys'], settings['servicePrivateFileName'], settings['p_str'], settings['g_str'])
	except SfsException as SfsEx:
		logger.error(str(SfsEx), exc_info=True)
		exit(1)
	except CryptoException as CryptoEx:
		logger.error(str(CryptoEx), exc_info=True)
		exit(2)

	address = ('0.0.0.0', settings['servicePort'])
	server = ThreadedTCPServer(address, ServiceServerHandler)
	server.serve_forever()